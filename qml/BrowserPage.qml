import QtQuick 2.9
import QtQuick.Controls 2.2
import QtWebEngine 1.7
import Morph.Web 0.1
import Lomiri.Components 1.3 as Lomiri
import BlobSaver 2.0

Page {
    WebContext {
        id: webcontext
        userAgent: 'Mozilla/5.0 (Linux; Ubuntu 20.04 like Android 9) AppleWebKit/537.36 Chrome/87.0.4280.144 Mobile Safari/537.36'
        offTheRecord: false

        userScripts: [
            BlobSaverUserScript {},
            WebEngineScript {
                injectionPoint: WebEngineScript.DocumentCreation
                sourceUrl: Qt.resolvedUrl('inject.js')
                worldId: WebEngineScript.MainWorld
            }
        ]
    }

    BlobSaverHandler {
        onFileDownloaded: {
            var exportPage = stackView.push(Qt.resolvedUrl('ExportPage.qml'));
            exportPage.path = path;

            exportPage.close.connect(function(files) {
                stackView.pop();
            });
        }
    }

    WebView {
        id: webview
        anchors.fill: parent

        context: webcontext
        url: 'https://www.photopea.com/'

        function navigationRequestedDelegate(request) {
            var url = request.url.toString();

            if (!url.match('(http|https)://www.photopea.com/(.*)') && request.isMainFrame) {
                Qt.openUrlExternally(url);
                request.action = WebEngineNavigationRequest.IgnoreRequest;
            }
        }

        onFileDialogRequested: function(request) {
            request.accepted = true;

            var importPage = stackView.push(Qt.resolvedUrl('ImportPage.qml'));
            importPage.allowMultipleFiles = (request.mode == FileDialogRequest.FileModeOpenMultiple);

            importPage.accept.connect(function(files) {
                request.dialogAccept(files);
                stackView.pop();
            });

            importPage.reject.connect(function() {
                request.dialogReject();
                stackView.pop();
            });
        }
    }

    ProgressBar {
        height: units.dp(3)
        anchors {
            left: parent.left
            right: parent.right
            top: parent.top
        }

        value: (webview.loadProgress / 100)
        visible: (webview.loading && !webview.lastLoadStopped)
    }

    Connections {
        target: Lomiri.UriHandler
        onOpened: {
            webview.url = uris[0];
        }
    }

    Component.onCompleted: {
        if (Qt.application.arguments[1] && Qt.application.arguments[1].indexOf('photopea.com') >= 0) {
            webview.url = Qt.application.arguments[1];
        }
    }
}
