function openFile() {
  console.log('going to open file');
  document.querySelector('input').click();
}

window.addEventListener('unhandledrejection', (event) => {
  // This is super hacky, but it seems like the only way to reliably get the open buttons to work

  if (event.reason == 'AbortError: The user aborted a request.') {
    openFile();
  }
});
