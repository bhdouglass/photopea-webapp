import QtQuick 2.9
import QtQuick.Controls 2.2

ApplicationWindow {
    id: root

    visible: true
    width: units.gu(50)
    height: units.gu(75)

    StackView {
        id: stackView
        anchors.fill: parent

        initialItem: BrowserPage {}
    }
}
