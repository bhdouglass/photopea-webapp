import QtQuick 2.9
import QtQuick.Controls 2.2
import Lomiri.Content 1.3
import BlobSaver 2.0

Page {
    id: openDialog

    property var activeTransfer
    property var path

    signal close()

    Rectangle {
        anchors.fill: parent

        ContentItem {
            id: exportItem
        }

        ContentPeerPicker {
            visible: openDialog.visible
            handler: ContentHandler.Destination
            contentType: ContentType.Pictures

            onPeerSelected: {
                activeTransfer = peer.request();
                var items = [];
                exportItem.url = path;
                console.log(path);
                items.push(exportItem);
                activeTransfer.items = items;
                activeTransfer.state = ContentTransfer.Charged;

                BlobSaver.remove(path);
                close();
            }

            onCancelPressed: {
                BlobSaver.remove(path);
                close();
            }
        }
    }
}
